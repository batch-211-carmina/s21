// console.log("Hello World!");


//An array in programming is simply a list of data.

	let studentNumberA = '2020-1923';
	let studentNumberB = '2020-1924';
	let studentNumberC = '2020-1925';
	let studentNumberD = '2020-1926';
	let studentNumberE = '2020-1927';

	let studentNumbers = ['2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927'];


//ARRAYS
	/*
		Arrays are used to store multiple values in a single variable
		The are declared using square brackets [] also known as 'Array Literals'

		SYNTAX:

		let/const arrayName = [elementA, elementB, elementC...];
	*/

	//Common examples of arrays

	let grades = [98.5, 94.3, 89.2, 90.1];
	let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

	console.log(grades);
	console.log(computerBrands);


	//Possible use of an array but it is not recommended

	let mixedArr = [12, 'Asus', null, undefined, {}];
	console.log(mixedArr);


	//Alternative way to write arrays

	let myTasks = [
		'drink html',
		'eat javascript',
		'inhale css',
		'bake sass'
	];
	console.log(myTasks);


	//MINI ACTIVITY #1

	let tasks = ['sleep', 'eat', 'work', 'online class', 'watch'];
	let capitalCitites = ['Manila', 'Tokyo', 'Bangkok', 'Seoul'];

	console.log(tasks);
	console.log(capitalCitites);

	let city1 = 'Tokyo';
	let city2 = 'Manila';
	let city3 = 'Jakarta';

	let cities = [city1, city2, city3];
	console.log(cities);


	//Length Property
	/*
		The .length property allows us to get and set the total number of items in an array.
	*/

	console.log(myTasks.length);  //output: 4
	console.log(cities.length);	  //output: 3

	let blankArr = [];
	console.log(blankArr.length);  //output: 0

	//length property can also be used with strings
	//some array methods and properties can also be used with strings

	let fullName = 'Jamie Noble';
	console.log(fullName.length);

	//length property can also set the total number of items in an array, meaning we can actually DELETE the last item in the array or shorten the array by simply updating the length property of an array

	myTasks.length = myTasks.length-1;
	console.log(myTasks.length);
	console.log(myTasks);

	//another example to delete an item in an array using decrementation
	cities.length--;
	console.log(cities);

	fullName.length = fullName.length-1;
	console.log(fullName.length);
	fullName.length--;
	console.log(fullName);


	let theBeatles = ['John', 'Paul', 'Ringo', 'George'];
	// theBeatles.length++;

	theBeatles[4] = 'Cardo';
	theBeatles[theBeatles.length] = 'Ely';
	theBeatles[theBeatles.length] = 'Chito';
	theBeatles[theBeatles.length] = 'MJ';
	console.log(theBeatles);


	//MINI ACTIVITY #2
	//Create a funcion called addTrainers that can receive a single argument
	//add the argument in the end of theTrainers array
	//invoke and add an argument to be passed in the function
	let theTrainers = ['Ash'];

	/*function addTrainers(trainer){
		theTrainers[theTrainers.length] = 'Brock';
		theTrainers[theTrainers.length] = 'Misty';
		console.log(theTrainers);
	}

	addTrainers();*/

	//solution
	function addTrainers(trainer){
		theTrainers[theTrainers.length] = trainer;
	}
	addTrainers('Misty');
	addTrainers('Brock');
	console.log(theTrainers);


	//Reading from Arrays
	/*
		- Accessing array elements is one of the more common tasks that we do with an array
		- This can be done through the use of array indexes
		- Each element in an array is associated with its own index/number

		- The reason an array starts with 0 is due to how the language is designed

		SYNTAX:
			arrayName[index];
	*/

	console.log(grades[0]);  //output: 98.5
	console.log(computerBrands[3]);  //output: Neo

	//accessing an array element that does not exist yet will return 'undefined'
	console.log(grades[20]);


	let lakersLegends = ['Kobe', 'Shaq', 'Lebron', 'Magic', 'Kareem'];
	console.log(lakersLegends[1]);
	console.log(lakersLegends[3]);

	let currentLaker = lakersLegends[2];
	console.log(currentLaker);
	console.log(currentLaker);

	console.log("Array before reassignment");
	console.log(lakersLegends);
	lakersLegends[2] = 'Pau Gasol';
	console.log("Array after reassignment");
	console.log(lakersLegends);



	//MINI ACTIVITY #3
	/*
		Create a function name findBlackMamba which is able to receive an index number as a single argument and return the item accessed by its index
		-function should be able to receive one arg
		-return the blackMamba accessed by the index
		-create a variable outside the function called blackMamba and store the value returned by the function in it
		-log the blackMamba variable in the console
	*/

	//solution
	function findBlackMamba(index) {
		return lakersLegends[index];
	}

	let blackMamba = findBlackMamba(0);
	console.log(blackMamba);


	/*let blackMamba = 'Kobe';
	function findBlackMamba(find) {
		blackMamba[blackMamba.length] = blackMamba;
	}
	console.log(blackMamba);*/




	//MINI ACTIVITY #4
	/*
		Update and reassign the last two items in the array with your own favorite food
	*/
	let favoriteFoods = [
		'Tonkatsu',
		'Adobo',
		'Hamburger',
		'Sinigang',
		'Pizza'
	];

	console.log(favoriteFoods);

	favoriteFoods[3] = 'Shawarma';
	favoriteFoods[4] = 'Lasagna'

	console.log(favoriteFoods);


	//Accessing the last element of an array

	let bullsLegends = ['Jordan', 'Pippen', 'Rodman', 'Rose', 'Kukoc'];

	let lastElementIndex = bullsLegends.length-1;
	console.log(lastElementIndex);  //output: 4
	console.log(bullsLegends.length);  //output: 5

	console.log(bullsLegends[lastElementIndex]);  //output: Kukoc
	console.log(bullsLegends[bullsLegends.length-1]);  //output: Kukoc


	//Adding items into the Array

	let newArr = [];
	console.log(newArr[0]);

	newArr[0] = 'Cloud Strife';
	console.log(newArr);

	console.log(newArr[1]);
	newArr[1] = 'Tifa Lockhart';
	console.log(newArr);

	// newArr[newArr.length-1] = 'Aerith Gainsborough';

	newArr[newArr.length] = 'Barret Wallace';
	console.log(newArr);


	//Looping over an Array

	for(let index = 0; index < newArr.length; index++){
		console.log(newArr[index]);
	}


	let numArr = [5,12,30,46,40];

	for(let index = 0; index < numArr.length; index++){

		if(numArr[index] % 5 === 0){
			console.log(numArr[index] + ' is divisible by 5');
		}
		else {
			console.log(numArr[index] + ' is not divisible by 5');
		}
	}


	//Multidimensional Arrays
	/*
		- Multidimensional Arrays are useful for storing complex data structures
		- a practical application of this is to help visualize/create real world objects
	*/

	let chessBoard = [
		['a1','b1','c1','d1','e1','f1','g1','h1'],
		['a2','b2','c2','d2','e2','f2','g2','h2'],
		['a3','b3','c3','d3','e3','f3','g3','h3'],
		['a4','b4','c4','d4','e4','f4','g4','h4'],
		['a5','b5','c5','d5','e5','f5','g5','h5'],
		['a6','b6','c6','d6','e6','f6','g6','h6'],
		['a7','b7','c7','d7','e7','f7','g7','h7'],
		['a8','b8','c8','d8','e8','f8','g8','h8']
	];

	console.log(chessBoard);

	//to access multidimensional array
	console.log(chessBoard[1][4]);
	console.log("Pawn moves to: " + chessBoard[1][5]);  //output: f2

	//MINI ACTIVITY #5 - to access a8 and h6
	console.log(chessBoard[7][0]);
	console.log(chessBoard[5][7]);